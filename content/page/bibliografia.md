---
title: Bibliografia
subtitle: Fontes utilizadas na criação desse trabalho
comments: false
---

My name is The Dude. I have the following qualities:

- [Tomada de decisões em sistemas financeiros utilizando algoritmos de aprendizado de máquina supervisionado](https://teses.usp.br/teses/disponiveis/55/55137/tde-22032019-171747/publico/LuisCarlosOtteJunior_revisada.pdf)
- [Análise de Tendências de Mercado por Redes Neurais Artificiais](https://fei.edu.br/~cet/cbrn05.pdf)
- [4 aplicações práticas do machine learning no setor financeiro](https://cantarinobrasileiro.com.br/blog/4-aplicacoes-praticas-do-machine-learning-no-setor-financeiro/)
