---
title: Sobre
subtitle: Repositório do conhecimento, IA no mercado financeiro
comments: false
---

Olá, esse trabalho foi feito pelos alunos Carlos Augusto e Leonardo Jorge para disciplina de Tecnologia da Informação e do Conhecimento do 8º período.

Para o tema foi escolhido mercado financeiro e esta pagina é um prototipo de Repositório de Conhecimento sobre inteligência articial no mercado.  

- Algotrading
- Backtesting
- Redes Neurais

