## Trabalho Final - Tecnologia da Informação e do Conhecimento

O mercado financeiro foi a escolha de área de conhecimento feita pelo grupo, pois é onde o grupo já tem familiaridade com o assunto.

O mercado financeiro tem adotado de forma crescente o uso de robôs e IA para investimento, no USA a maior parte dos investidores já são robôs automatizados e a tendência é com o crescimento do mercado financeiro, essa também se tornar uma realidade brasileira.

O foco é automatizar estrategias ou conseguir gerar analises e previsões de preços de ações para gerar maior renda nas negociações e remover o fator emocional no investimento.

As fontes utilizadas para a busca das tecnologias foram feitas por meio de conhecimento pessoal dos componentes do grupo com a experiência na área escolhida, consulta a outros profissionais da área e também pesquisa em artigos e teses acadêmicas.
