---
title: Análise de Tendências de Mercado por Redes Neurais Artificiais 
subtitle: Uma abordagem de mercado usando redes neurais
date: 2021-06-07
tags: ["Redes neurais", "Análise", "IA"]
---

Este trabalho tem por objetivo avaliar, através de
um modelo numérico estrutural simplificado, uma
metodologia de análise de tendências de curto prazo
para série de preços de ativos financeiros. Um ativo
financeiro pode apresentar, basicamente, três tipos de
tendência : alta, baixa e sem tendência. Desta forma, o
modelo em questão deve determinar as tendências de
curto prazo das ações de TELEBRAS PN e, ainda mais
importante, as suas mudanças. A rede utiliza como
dados de entrada as variações percentuais diárias das
cotações médias de TELEBRAS PN, obtidas através de
um banco de dados histórico. O algoritmo neural
implementado para esta aplicação é o Self-Organizing
Network (Competitive Learning), cujo mapeamento
classifica os vetores de entrada nas três possíveis
tendências já descritas. Para executar esta Rede
Neural, o usuário deve dispor do software MatLab
(versão 4.2 ou superior) com a caixa de ferramentas
de redes neurais (Neural Network Toolbox) instalada.


Fonte: [Análise de Tendências de Mercado por Redes Neurais Artificiais](https://fei.edu.br/~cet/cbrn05.pdf)
