---
title: machine learning no setor financeiro
subtitle: 4 aplicações práticas 
date: 2021-06-07
tags: ["IA", "automação"]
---

O uso do aprendizado de máquina na gestão de diferentes modelos de negócio ainda é uma novidade recente. Na prática, o uso da tecnologia apresenta uma série de desafios. Contudo, as vantagens de longo prazo certamente superam muito os obstáculos.


Fonte: [machine learning no setor financeiro](https://cantarinobrasileiro.com.br/blog/4-aplicacoes-praticas-do-machine-learning-no-setor-financeiro/)
